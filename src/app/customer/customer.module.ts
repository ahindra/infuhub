import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { 
  MatButtonModule, 
  MatCheckboxModule,
  MatCardModule, 
  MatDialogModule, 
  MatInputModule,
  MatTableModule,
  MatToolbarModule, 
  MatMenuModule,
  MatIconModule, 
  MatProgressSpinnerModule,
  MatSidenavModule
} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    CustomerRoutingModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatCardModule, 
    MatDialogModule, 
    MatInputModule,
    MatTableModule,
    MatToolbarModule, 
    MatMenuModule,
    MatIconModule, 
    MatProgressSpinnerModule,
    MatSidenavModule
  ],
  declarations: [
    DashboardComponent, 
    ProfileComponent,
    CustomerComponent,
  ],
// exports: [
//   CustomerComponent, 
//   DashboardComponent, 
//   ProfileComponent,
// ]
  
})
export class CustomerModule { }
