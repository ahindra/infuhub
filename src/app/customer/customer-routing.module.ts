import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerComponent } from './customer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';


const routes:Routes = [
    {
        path: '',
        component: CustomerComponent,
        children: [ 
            { path: 'dashboard', component: DashboardComponent  },
            { path: 'profile', component: ProfileComponent } 
        ]
    }
]



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class CustomerRoutingModule { }
