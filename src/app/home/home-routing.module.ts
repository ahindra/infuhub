
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HelpComponent } from './help/help.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
//import { InfluencersComponent } from './influencers/influencers.component';


const HomeRouting: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent },
            { path: 'help', component: HelpComponent },
            { path: 'pricing', component: PricingComponent },
            { path: 'blog', component: BlogComponent }
            
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(HomeRouting)],
    exports: [RouterModule]
  })

export class HomeRoutingModule{ }
