import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HelpComponent } from './help/help.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
//import { InfluencersComponent } from './influencers/influencers.component';

@NgModule({
  declarations: [
    LoginComponent, 
    SignupComponent, 
    HomeComponent, 
    HelpComponent, 
    PricingComponent, 
    BlogComponent, 
    //InfluencersComponent,
    
  ],

  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,

  ],
})



export class HomeModule { }
