import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  constructor() {
    
   }

  ngOnInit() {
    //mobile nav toggle
    let mobileNav = document.getElementById('mobileNav');
    let openNav = document.getElementById('collapsibleNavbar');
    mobileNav.addEventListener('click', function(){
      openNav.classList.toggle('show');
    });
  }

}
